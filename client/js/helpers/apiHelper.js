const API_URL = 'http://localhost:3000/';
const jwt = localStorage.getItem('jwt');


function callApi(endpoind, method) {
  const url = API_URL + endpoind;
  const options = {
    method,
    headers: {
      'Authorization': `Bearer ${jwt}`
    }
  };

  return fetch(url, options)
    .then(response =>
      response.ok ? response.json() : Promise.reject(Error('Failed to load'))
    )
    .catch(error => {
      throw error;
    });
}

export { callApi };