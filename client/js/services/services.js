function countSpeed(time, progress) {
  if (!progress) return 0;

  return Math.round(progress * 60 / time);
}

function checkResult(text, input) {
  const inputArr = input.split('');
  const textArr = text.split('');
  let progress, mistake;

  for (let i = 0; i < inputArr.length; i++) {
    if (inputArr[i] === textArr[i]) {
      progress = i + 1;
    } else {      
      mistake = i + 1;
    }
  }

  if (inputArr.length === textArr.length) {
    return {done: true};
  }

  return {progress, mistake};
}

export { countSpeed, checkResult };