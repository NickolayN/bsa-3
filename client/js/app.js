import {
  gameService
} from "./services/gameService.js";
import Game from './game.js';
import showStartScreen from './views/startScreen.js';

import timeToNextRace from './views/timeToNextRaceView.js';
import renderStatistic from './views/statisticView.js';
import showMessage from './views/showMessage.js';

class App {
  constructor() {
    this.initGame();
  }

  startGame() {    
    this.game.start();
  }

  initGame() {
    const jwt = localStorage.getItem('jwt');
    const socket = io.connect(`http://localhost:3000?token=${jwt}`);

    this.game = new Game();
    this.game.onProgress = (progress) => {
      socket.emit('progress', {race: this.raceId, progress});
    }

    socket.on('connected', races => {
      showStartScreen(socket, races);
    });

    socket.on('newRace', (races) => showStartScreen(socket, races));

    socket.on('joinedToRace', async ({ raceId, time, textId }) => {
      this.raceId = raceId;
      App.showTimeToStart(time);

      try {
        this.game.text = await gameService.getText(textId);
      } catch (error) {
        console.log(error);
      }      
    });

    socket.on('startGame', () => this.startGame());

    socket.on('playerJoined', (payload) => {
      this.game.players = payload;
    });

    socket.on('progress',players => {
      this.game.updatePlayers(players);
    });

    socket.on('active', races => {
      if (this.raceId) return;
      showStartScreen(socket, races);
    });

    socket.on('finishGame', races => renderStatistic(races));

    socket.on('message', message => showMessage(message));
  }

  static showTimeToStart(time) {
    const seconds = Math.floor((time - Date.now()) / 1e3);
    root.append(timeToNextRace(seconds));
  }
}

new App();