function showStartScreen(socket, races) {
  const template = (races = 'No races') => `<h1>Join to race or create new one!</h1>
  <div class="new-game-wrapper">
    <button id="newGame">Start new game</button>
  </div>
  <div>
    <h2>Choose a race:</h2>
    <ul class="races-list" id="racesList">${races}</ul>
  </div>`;

  let mappedRaces;

  if (races) {
    mappedRaces = races.map(race => {
      return `<li>
      <button data-race="${race}">Race: ${race}</button>
    </li>`;
    });
  }

  root.innerHTML = template(mappedRaces);

  newGame.onclick = () => {
    socket.emit('createRace');
  };

  racesList.onclick = (event) => {
    const target = event.target.closest('[data-race]');
    if (!target) return;
    socket.emit('joinRace', target.dataset.race);
  }
}

export default showStartScreen;