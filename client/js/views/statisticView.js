function renderStatistic(players) {
  const ul = document.createElement('ul');
  const playersStr = players.sort((a, b) => b.progress - a.progress)
    .map(player => `<li>${player.login}: ${player.progress}</li>`);
  ul.innerHTML = playersStr;

  root.innerHTML = "";
  root.append(ul);
}

export default renderStatistic;