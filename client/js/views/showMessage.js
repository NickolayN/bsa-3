function showMessage(message) {
  let messagesList = document.getElementById('messagesList');
  if (!messagesList) {
    messagesList = createChat();
  }

  const messageElem = createMessage(message);
  messagesList.append(messageElem);
  scrollIntoView(messagesList);
}

function createChat() {
  const chat = document.createElement('div');
  chat.className = 'chat';
  const messagesList = document.createElement('ul');
  messagesList.id = 'messagesList';

  chat.append(messagesList);
  root.append(chat);
  return messagesList;
}

function createMessage(message) {
  const messageElem = document.createElement('li');
  messageElem.innerHTML = message;
  return messageElem;
}

function scrollIntoView(elem) {
  const parent = elem.parentNode;
  parent.scrollTop = parent.scrollTop + parent.clientHeight;
}

export default showMessage;