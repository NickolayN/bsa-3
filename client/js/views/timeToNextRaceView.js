function timeToNextRace(time) {
  const elem = document.createElement('div');
  elem.innerText = 'The race will start: '
  const timer = document.createElement('span');
  elem.appendChild(timer);
  timer.setAttribute('id', 'timer');
  timer.innerText = time;
  const timerId = setInterval(() => {
    time--;
    timer.innerText = time;
    if (time <= 0) clearInterval(timerId);    
  }, 1000);

  root.innerHTML = '';
  root.append(elem);
}

export default timeToNextRace;