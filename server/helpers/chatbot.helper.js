function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

const randomPhrases = () => {
  const phrases = [
    'Adventure seeker on an empty street, Just an alley creeper, light on his feet',
    'A young fighter screaming, with no time for doubt',
    'With the pain and anger can’t see a way out',
    '"It ain’t much I’m asking," I heard him say',
    '"Got to find me a future. Move out of my way"',
    'I want it all, I want it all, I want it all, And I want it now',
    'Listen all you people, come gather round, Just give me what I know is mine',
    'People do you hear me? Just give me the sign',
    'It ain’t much I’m asking, if you want the truth',
    'Here’s to the future for the dreams of youth'
  ]

  const random = getRandomInt(0, phrases.length);
  return phrases[random];
}

const createPhrase = (() => {
  const types = {
    introduceYourself() {
      return 'На улице сейчас немного пасмурно, но на Львов Арена сейчас просто замечательная атмосфера: двигатели рычат, зрители улыбаются а гонщики едва заметно нервничают и готовят своих железных коней до заезда. А комментировать это все действо Вам буду я, Эскейп Ентерович и я рад вас приветствовать со словами Доброго Вам дня господа!';
    },

    introducePlayers({players}) {
      return `На стартовой линии:
      <ul>${players.map(player => `<li>${player.name}</li>`)}</ul>`
    },

    newPlayer({player}) {
      return `К старту приближается: ${player}`;
    },

    currentPosition({players}) {
      const positions = ['первым идет', 'вторым', 'замыкает тройку', 'далее:']
      return players.map((player, i) => {
        return `${positions[i] ? positions[i] : ''} ${player.name}`
      }).join(', ');
    },

    beforeFinish({players}) {
      return `До финиша осталось совсем немного и похоже что первым
        его может пересечь ${players[0].name} из команды Atom на своем белом BMV.
        ${players[1] ? `Второе место может достаться${players[1].name}.` : ''} Но давайте дождемся финиша.`;
    },

    onFinish({player, position = 'default'}) {
      const positions = {
        first: 'Финишную прямую пересекает',
        second: 'Финиширует на второй позиции',
        third: 'Замыкает тройку лидеров',
        default: 'Завершает гонку',
        last: 'Последним завершает гонку'
      };

      return `${positions[position]} ${player}`;
    },

    finish({players}) {
      return `Наша гонка завершена, и мы можем подвести итоги:<ul>
      ${players.map(player => {
        return `<li>${player.name}</li>`
      }).join('')}</ul>`
    },

    randomly() {
      return randomPhrases();
    }
  }

  const unknown = (type) => types.randomly();

  return (type = randomly, data) => {
    const creator = types[type];
    return creator ? creator(data) : unknown(type);
  }
})();

module.exports = {createPhrase};
