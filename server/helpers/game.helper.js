const TEXTS = 3;

const handlers = Symbol('handlers');

function makeObservable(target) {
  target[handlers] = new Set();

  target.observe = (handler) => {
    target[handlers].add(handler);
  };

  return new Proxy(target, {
    set(target, property, value, receiver) {
      const success = Reflect.set(...arguments);
      if (success) {
        target[handlers].forEach(handler => handler(property, value));
      }

      return success;
    }
  });
}

function getTextId() {
  return getRandom(0, TEXTS);
}

function getRandom(min, max) {
  const rand = min - 0.5 + Math.random() * (max - min + 1);
  return Math.round(rand);
}

module.exports = {makeObservable, getTextId};