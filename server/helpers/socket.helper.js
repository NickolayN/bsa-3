const userRepository = require('../repositories/users.repository');
const jwt = require('jsonwebtoken');


function verifyUser(user) {
  const userInDB = userRepository.getUserByLogin(user.login);
  return (userInDB && userInDB.password === user.password) ? user : null;
}

function getUserFromSocket(socket) {
  const token = socket.handshake.query.token;
  return user = jwt.verify(token, 'secret');
}

module.exports = {verifyUser, getUserFromSocket};