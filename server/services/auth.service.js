const jwt = require('jsonwebtoken');
const {verifyUser} = require('../helpers/socket.helper');

function login(user) {
  if (verifyUser(user)) {
    const token = jwt.sign(user, 'secret');
    return token;
  }
  
  return null;
}

module.exports = {
  login
}