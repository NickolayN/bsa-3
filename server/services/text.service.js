const textRepository = require('../repositories/text.repository');

function getText(id) {
  return textRepository.getText(id);
}

module.exports = {getText};