const {
  getUserFromSocket
} = require('../helpers/socket.helper');
const Race = require('./race.service');
const Chatbot = require('./chatbot.service');

const {makeObservable, getTextId} = require('../helpers/game.helper');
const MESSAGE_INTERVAL = 3e4;

class GameService {
  constructor(io) {
    this.io = io;
    this.races = new Map();
  }

  init() {
    this.io.on('connection', (socket) => {
      socket.emit('connected', this.getRaces());

      socket.on('createRace', () => this.createRace(socket));
      socket.on('joinRace', raceId => this.joinRace(socket, raceId));
      socket.on('progress', (payload) => {
        const raceId = payload.race;
        const race = this.getRace(raceId);
        race.onProgress(getUserFromSocket(socket).login, payload.progress);
        
        this.io.to(raceId).emit('progress', race.players);
      });
    });
  }

  getRaces() {
    if (!this.races.size) {
      return null;
    }

    const races = [...this.races.entries()]
      .filter(race => {
        return !race[1].isActive})
      .map(race => race[0]);
    return races.length > 0 ? races : null;
  }

  getRace(raceId) {
    return this.races.get(raceId);
  }

  createRace(socket) {
    const raceId = socket.id;
    let race = new Race(raceId);
    race.textId = getTextId();
    race = makeObservable(race);

    const chatbot = new Chatbot(race.textId);

    race.observe((key, value) => {
      if (key === 'state') {
        const comment = chatbot.say(value);
        if (comment) {
          this.io.to(raceId).emit('message', comment);
        }
      };
    });
    
    this.races.set(raceId, race);
    socket.broadcast.emit('newRace', this.getRaces());

    race.onStart = () => {
      this.io.to(raceId).emit('startGame');
      socket.broadcast.emit('active', race.getRaces);
      
      this.io.to(raceId).emit('message', chatbot.intoduceYourself());

      this.chatbotTimer = setInterval(() => {
        const message = chatbot.sayRandomly();
        this.io.to(raceId).emit('message', message)
      }, MESSAGE_INTERVAL);
    };

    race.onFinish = async () => {
      await this.io.to(raceId).emit('finishGame', race.players);
      socket.leave(raceId);
      this.races.delete(raceId);

      clearInterval(this.chatbotTimer);
    }

    this.joinRace(socket, raceId);
  }

  joinRace(socket, raceId) {
    socket.join(raceId);
    const race = this.getRace(raceId);
    race.addPlayer(getUserFromSocket(socket).login);

    socket.emit('joinedToRace', {
      raceId,
      time: race.startTime,
      textId: race.textId,
      players: race.players
    });
    this.io.to(raceId).emit('playerJoined', race.players);
  }
}

module.exports = GameService;