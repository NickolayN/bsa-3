const {getUserFromSocket} = require('../helpers/socket.helper');
const TIME_TO_START = 3e4;
const RACE_TIME = 9e4;

class Race {
  constructor(id, textId = 0) {
    this.id = id;
    this.startTime = Date.now() + TIME_TO_START;
    this.textId = textId;

    this.state = {
      isActive: false,
      players: new Map()
    };

    this.init();
  }

  get isActive() {
    return this.state.isActive;
  }

  set isActive(state) {
    this.state = {...this.state, ...{isActive: state}};
  }

  get players() {
    return [...this.state.players.entries()]
      .map(player => {
        return {login: player[0], progress: player[1].progress}
      });
  }

  init() {
    setTimeout(() => this.start(), TIME_TO_START);
  }

  addPlayer(player, progress = 0) {
    const players = new Map([...this.state.players.entries()]);
    players.set(player, {progress});
    this.state = {...this.state, players};
  }

  start() {
    this.isActive = true;
    this.onStart();

    setTimeout(() => this.onFinish(), RACE_TIME);
  }
  
  onStart() {}

  onProgress(user, progress) {
    this.addPlayer(user, progress);
  }

  onFinish() {};
}

module.exports = Race;