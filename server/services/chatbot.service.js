const {createPhrase} = require('../helpers/chatbot.helper');
const { getText } = require('../repositories/text.repository');

const CONTROL_POINT = 30;

class Chatbot {
  constructor(textId) {
    this.state = {
      history: []
    }
    getText(textId)
      .then(text => this.textLength = text.length);
  }

  get history() {
    return [...this.state.history];
  }

  set history(state) {
    const history = this.history;
    history.push(state);
    this.state = {...this.state, history};
  }

  get players() {
    if (!this.history.length) return [];

    return [...this.history[this.history.length - 1].players.entries()]
      .map(( [player, {progress}] ) => ({name: player, progress}));
  }

  get sortedPlayers() {
    return this.players.sort((a, b) => b.progress - a.progress);
  }

  get prevStagePlayers() {
    if (this.history.length < 2) return [];

    const players = this.history[this.history.length - 2].players.entries();
    return [...players].map(( [player, {progress}] ) => ({name: player, progress}));
  }

  checkDiff(players, prevPlayers) {
    const diff = [];
    players.forEach((player, i) => {
      if (player.progress !== (prevPlayers[i] && prevPlayers[i].progress)) {
        diff.push(player);
      }
    });

    return diff;
  }

  say({...state}) {
    this.history = state;
    
    const currentPosition = this.history[this.history.length - 1];
    const prevPosititon = this.history[this.history.length - 2];
    const diff = this.checkDiff(this.players, this.prevStagePlayers);

    diff.some( ({progress}) => {
      progress === this.textLength - CONTROL_POINT;
    });

    if (currentPosition.isActive && !prevPosititon.isActive) {
      const players = this.players;
      return createPhrase('introducePlayers', {players});
    }
    
    if ( diff.some( ({progress}) => {
      return progress === this.textLength - CONTROL_POINT;
    }) ) {
      const players = this.sortedPlayers;      
      return createPhrase('beforeFinish', {players});
    }
    
    if ( diff.some( ({progress}) => {
      return progress === this.textLength - 1;
    })) {
      const player = diff.find(player => player.progress === this.textLength - 1);
      return createPhrase('onFinish', {player: player.name});
    }
  }

  intoduceYourself() {
    return createPhrase('introduceYourself');
  }

  finish() {
    clearInterval(this.timerId);
    return createPhrase('finish');
  }

  sayRandomly() {
    return createPhrase('randomly');
  }
}

module.exports = Chatbot;