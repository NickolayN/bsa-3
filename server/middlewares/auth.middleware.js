const passport = require('passport');

require('../passport.config');

function authMiddleware() {
  return passport.authenticate('jwt', {session: false});
}

module.exports = authMiddleware;