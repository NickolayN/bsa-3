const path = require('path');
const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const bodyParser = require('body-parser');
const socketMiddleware = require('./middlewares/socket.middleware');


app.use(bodyParser.json());

const indexRouter = require('./routes/index');
const textRouter = require('./routes/text');
const loginRouter = require('./routes/login');

app.use(express.static(path.join(__dirname, '../client/')));
app.use('/', indexRouter);
app.use('/text', textRouter);
app.use('/login', loginRouter);

io.use(socketMiddleware);


const GameService = require('./services/game.service');
const game = new GameService(io);
game.init();





server.listen(3000);