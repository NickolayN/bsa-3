const fs = require('fs');

function getUserByLogin(login) {
  const rawData = fs.readFileSync('./data/users.json');
  const usersList = JSON.parse(rawData);

  return usersList.find(user => user.login === login);
}

module.exports = {
  getUserByLogin
}