const fs = require('fs');

async function getText(id) {
  const rawData = await fs.readFileSync('./data/texts.json');
  const textsList = await JSON.parse(rawData);

  const text = textsList[id];
  return text;
}

module.exports = {
  getText
}